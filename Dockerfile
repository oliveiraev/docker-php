FROM php:7.1-fpm-alpine

ARG error_file=/var/log/php7.1-fpm/error.log
ARG log_file=/var/log/php7.1-fpm/www.access.log

RUN mkdir -p $(dirname $error_file) \
&& rm -f $error_file && mkfifo -m006 $error_file \
&& rm -f $log_file && mkfifo -m006 $log_file

COPY usr/local/bin/piped-std /usr/local/bin/piped-std
ENTRYPOINT ["/usr/local/bin/piped-std", "/usr/local/bin/docker-php-entrypoint"]
CMD ["php-fpm"]

RUN apk add --no-cache --virtual .php-deps \
    icu \
    libintl \
    libpng \
    libxslt \
    openssl \
    sqlite-libs \
&& apk add --no-cache --virtual .php-build-deps \
    curl-dev \
    gettext-dev \
    icu-dev \
    libedit-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    libxslt-dev \
    openssl-dev \
    sqlite-dev \
    readline-dev \
&& docker-php-source extract \
&& find /usr/src/php/ext/ -maxdepth 2 -mindepth 2 -type f -name config?.m4 | while read conf; do cp $conf $(echo $conf | sed -r 's/config.\.m4/config.m4/'); done \
&& CFLAGS="-I/usr/src/php" docker-php-ext-install \
    dom \
    calendar \
    ctype \
    curl \
    exif \
    fileinfo \
    ftp \
    iconv \
    json \
    mbstring \
    mysqli \
    openssl \
    pdo \
    pdo_mysql \
    phar \
    posix \
    readline \
    simplexml \
    sockets \
    sqlite3 \
    shmop \
    sysvmsg \
    sysvsem \
    sysvshm \
    tokenizer \
    wddx \
    xml \
    xmlreader \
    xmlwriter \
&& PHP_OPENSSL_DIR=yes CFLAGS="-I/usr/src/php" docker-php-ext-install \
    gd \
    gettext \
    intl \
    pdo_sqlite \
    pdo_mysql \
    xsl \
&& docker-php-source delete \
&& apk del .php-build-deps
